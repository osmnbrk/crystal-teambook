import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Topics {

	private static LinkedList<Topic> topics = new LinkedList<Topic>();

	public Topics() {
		// TODO Auto-generated constructor stub
	}

	// Personal_Notes gibi hatta hemen hamen ayn�(fonk. isimleri faan)
	// Update methodu

	public static boolean get_all_topics(Session session) {

		try {

			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote/"
					+ session.team + "/topics";

			String jsonString = Connection.get(url);

			JSONObject jsonTopics = new JSONObject(jsonString); // Elde edilen string json objeye d�n��t�r�l�yor

			JSONArray jsonTopicsArr = jsonTopics.getJSONArray("Topics"); // Json bi�iminde topics array olu�turuluyor

			int arrLength = jsonTopicsArr.length();

			for (int i = 0; i < arrLength; i++) { // topics array i�inde d�ng�

				JSONObject jsontopic = jsonTopicsArr.getJSONObject(i); // jason bi�iminde 1 tane topic elde ediliyor

				topics.add(jsontopic_to_topic(jsontopic)); // jsoin bi�imindeki topic Topic objesine d�n��t�r�l�p topics
															// listesine ekleniyor

			}

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;

		}

	}

	public static boolean new_topic(Session session, Topic topic) {

		try {

			String input = "data=" + topic.data + "&time=" + topic.time + "&author_id=" + topic.author_id + "&title=" + topic.id + "&author=" + topic.author + "&type=" + topic.type;
			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote/"
					+ session.team + "/topics"; // sondaki personalnotes k�sm�ndan emin
																			// de�ilim

			Connection.post(url, input);

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;

		}

	}
	
	public static boolean new_comment(Session session, Topic topic, Comment comment) {
		
		try {
			
			String input = "data=" + comment.data + "&author_id=" + session.id + "&id=" + topic.id;
			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote/" + session.team + "/topics/" + topic.id;
			
			Connection.post(url, input);
			
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
			
		}
		
	} 

	private static Topic jsontopic_to_topic(JSONObject jsontopic) {

		String time = jsontopic.getString("time");
		String id = jsontopic.getString("id");
		String author = jsontopic.getString("author");
		String type = jsontopic.getString("type");

		JSONArray jsonCommentsArr = jsontopic.getJSONArray("message"); // Jason bi�iminde comments array
																		// olu�turuluyor(Json bi�imindeki topic nesnesi
																		// i�inden)
		
		String data = jsonCommentsArr.getJSONObject(0).getString("data");
		String author_id = jsonCommentsArr.getJSONObject(0).getString("author_id");

		int commentsLength = jsonCommentsArr.length();

		LinkedList<Comment> comments = new LinkedList<Comment>();

		for (int i = 1; i < commentsLength; i++) {

			JSONObject jsoncomment = jsonCommentsArr.getJSONObject(i);

			comments.add(jsoncomment_to_comment(jsoncomment));

		}

		Topic topic = new Topic(data, time, author_id, id, author, type, comments);

		return topic;

	}

	private static Comment jsoncomment_to_comment(JSONObject jsoncomment) {

		String data = jsoncomment.getString("data");
		String author_id = jsoncomment.getString("author_id");

		Comment comment = new Comment(data, author_id);

		return comment;

	}

	public static LinkedList<Topic> getTopics() {
		return topics;
	}

	public static void setTopics(LinkedList<Topic> topics) {
		Topics.topics = topics;
	}

}
