import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

public class DailyScrum {
	
	private static LinkedList<Todo> dailyscrum = new LinkedList<Todo>();

	public DailyScrum() {
		// TODO Auto-generated constructor stub
	}
	
	public static boolean get_all_todos(Session session) {
		
		try {
			
			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote" + session.team + "/dailyscrums";
			
			String jsonString = Connection.get(url);
			
			JSONObject jsonTodos = new JSONObject(jsonString).getJSONArray("DailyNotes").getJSONObject(0);  //DE���T�R�LECEK(T�M G�NLER� �EK�YOR)
			
			JSONArray jsonTodosArr = jsonTodos.getJSONArray("data");
			
			int arrLength = jsonTodosArr.length();
			
			for (int i = 0; i < arrLength; i++) {
				
				JSONObject jsonTodo = jsonTodosArr.getJSONObject(i);
				
				dailyscrum.add(jsontodo_to_todo(jsonTodo));
				
			}
			
			return true;
			
		} catch (Exception e) {
			// TODO: handle exception
			return false;
			
		}
		
	}
	
	private static Todo jsontodo_to_todo(JSONObject jsonTodo) {
		
		String data = jsonTodo.getString("data");
		String state = jsonTodo.getString("state");
		
		Todo todo = new Todo(data, state);
		
		return todo;
		
	}
	
	public static LinkedList<Todo> getDailyscrum() {
		return dailyscrum;
	}
	
	//Update methodu

}
