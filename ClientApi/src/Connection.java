import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.OutputStream;
import org.json.*;

import javax.net.ssl.HttpsURLConnection;

public class Connection {

	public Connection() {
		// TODO Auto-generated constructor stub
	}
	
	/*public static void main(String[] args) {
		
		String json = get("https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote/Team_1/dailyscrums");
		JSONObject obj = new JSONObject(json);
		
		JSONArray arr = obj.getJSONArray("DailyNotes").getJSONObject(0).getJSONArray("data");
		
		int a = arr.length();
		
		System.out.println(a);
		
		for(int i = 0; i < a; i++) {
			
			System.out.println(arr.getJSONObject(i).getString("state"));
			
		}	
		
	}*/
	

	public static String get(String urlString) {

		try {

			URL url = new URL(urlString);
			HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {

				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());

			}

			BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String incoming, output = "";
			System.out.println("Output from Server ... \n");
			while ((incoming = br.readLine()) != null) {

				System.out.println(incoming);
				output = output + incoming;
				
			}
			
			conn.disconnect();

			return output;

		} catch (IOException e) {

			e.printStackTrace();
			return null;

		}

	}

	public static String post(String urlString,String input) {

		try {
			
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			
			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String incoming, output = "";
			System.out.println("Output from Server .... \n");
			while ((incoming = br.readLine()) != null) {
				
				System.out.println(incoming);
				output = output + incoming;
				
			}
			
			conn.disconnect();
			
			return output;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return null;
			
		}

	}
	
	public static String post(String urlString) {

		try {
			
			URL url = new URL(urlString);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");

			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));

			String incoming, output = "";
			System.out.println("Output from Server .... \n");
			while ((incoming = br.readLine()) != null) {
				
				System.out.println(incoming);
				output = output + incoming;
				
			}
			
			conn.disconnect();
			
			return output;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			return null;
			
		}

	}

}
