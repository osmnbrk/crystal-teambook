import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Team {

	private static LinkedList<Member> team = new LinkedList<Member>();

	public Team() {
		// TODO Auto-generated constructor stub
	}

	public static boolean get_all_members(Session session) {

		try {

			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote" + session.team + "/members";
			
			String jsonString = Connection.get(url);

			JSONArray jsonMembersArr = new JSONObject(jsonString).getJSONArray("Community membe");

			int arrLength = jsonMembersArr.length();

			for (int i = 0; i < arrLength; i++) {

				JSONObject jsonMember = jsonMembersArr.getJSONObject(i);

				team.add(jsonMember_t_Member(jsonMember));

			}

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;

		}

	}
	
	private static Member jsonMember_t_Member(JSONObject jsonMember) {
		
		String telephone_number = jsonMember.getString("telephone_number");
		String community_id = jsonMember.getString("community_id");
		String surname = jsonMember.getString("surname");
		String mail = jsonMember.getString("mail");
		String address = jsonMember.getString("address");
		String team_id = jsonMember.getString("team_id");
		String id = jsonMember.getString("id");
		
		Member member = new Member(telephone_number, community_id, surname, mail, address, team_id, id);
		
		return member;
		
	}

}
