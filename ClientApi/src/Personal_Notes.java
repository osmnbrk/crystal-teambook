import java.util.LinkedList;

import org.json.JSONArray;
import org.json.JSONObject;

public class Personal_Notes {

	private static LinkedList<Personal_Note> personal_notes = new LinkedList<Personal_Note>();

	public Personal_Notes() {
		// TODO Auto-generated constructor stub
	}

	public static boolean get_all_notes(Session session) {

		try {

			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/"
					+ session.team + "/" + session.id + "/personal_notes";

			String jsonString = Connection.get(url);

			JSONArray jsonNotesArr = new JSONObject(jsonString).getJSONArray("Notes");

			int arrLength = jsonNotesArr.length();

			for (int i = 0; i < arrLength; i++) {

				JSONObject jsonNote = jsonNotesArr.getJSONObject(i);

				personal_notes.add(jsonNote_to_Note(jsonNote));

			}

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;

		}

	}

	public static boolean new_note(Session session, Personal_Note pN) {

		try {

			String input = "title=" + pN.title + "&data=" + pN.data + "&user_id=" + pN.user_id;
			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote/"
					+ session.team + "/" + session.id + "/personalnotes"; // sondaki personalnotes k�sm�ndan emin
																			// de�ilim

			Connection.post(url, input);

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;

		}

	}

	public static boolean delete_note(Session session, String id) {

		try {

			String url = "https://2qtsenzmv0.execute-api.eu-west-1.amazonaws.com/Test_Stage/Uludag_Universty/CristalNote/"
					+ session.team + "/" + session.id + "/personalnotes/" + id; // sondaki personalnotes k�sm�ndan emin
																			// de�ilim

			Connection.post(url);

			return true;

		} catch (Exception e) {
			// TODO: handle exception
			return false;

		}

	}

	private static Personal_Note jsonNote_to_Note(JSONObject jsonNote) {

		String data = jsonNote.getString("data");
		String title = jsonNote.getString("id");
		String user_id = jsonNote.getString("member_id");

		Personal_Note note = new Personal_Note(data, title, user_id);

		return note;

	}

	// Get all notes methodu olacak, parametre olarak session alacak.
	// delete note methodu, ayn� parametrelere ek olarak personal_note ya da idsini
	// falan alacak
	// new note methodu, ayn� parametreye ek olarak, personal_note alacak
	// Update methodu

}
