package com.uludag.berkay.crystalteambook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import java.util.Objects;

public class ReadNoteActivity extends AppCompatActivity {

    TextView title_tv, message_tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_note);
        Objects.requireNonNull(getSupportActionBar()).hide();

        title_tv = findViewById(R.id.read_note_title);
        message_tv = findViewById(R.id.read_note_message);

        Intent intent = getIntent();
        String title = intent.getStringExtra("title");
        String message = intent.getStringExtra("data");

        title_tv.setText(title);
        message_tv.setText(message);


    }



}
