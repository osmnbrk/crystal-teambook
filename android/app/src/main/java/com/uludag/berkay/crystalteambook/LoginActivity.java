package com.uludag.berkay.crystalteambook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.util.Objects;

public class LoginActivity extends AppCompatActivity {

    Button login_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Objects.requireNonNull(getSupportActionBar()).hide();

        login_button = this.findViewById(R.id.login_button);

        login_button.setOnClickListener((View v) -> {
            Intent intent = new Intent(this, MainActivity.class);
            this.startActivity(intent);
        });
    }
}
