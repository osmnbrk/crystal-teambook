package com.uludag.berkay.crystalteambook.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.uludag.berkay.crystalteambook.Models.Todo;
import com.uludag.berkay.crystalteambook.R;
import com.uludag.berkay.crystalteambook.ReadNoteActivity;
import com.uludag.berkay.crystalteambook.ReadTopicActivity;

import java.util.LinkedList;

class Holder extends RecyclerView.ViewHolder{

    private ImageView stateImage;
    private ImageView NotifyImage;
    private TextView text;
    private View itemView;

    Holder(@NonNull View itemView) {
        super(itemView);

        this.stateImage = itemView.findViewById(R.id.checkItem_state_img);
        this.NotifyImage = itemView.findViewById(R.id.check_item_notifyimg);
        this.text = itemView.findViewById(R.id.check_item_text);
        this.itemView = itemView;

        itemView.setOnLongClickListener((View v) -> {
            this.NotifyImage.setVisibility(View.VISIBLE);
            return true;
        });

    }

    void setDatas(Todo data){
        this.stateImage.setImageResource(R.drawable.ds_todo);

        if (!data.notify){
            this.NotifyImage.setVisibility(View.INVISIBLE);
        }

        this.text.setText(data.data);
    }

}

public class TodoListAdapter extends RecyclerView.Adapter<Holder> {

    LinkedList<Todo> dataset;
    LayoutInflater inflater;
    Context context;

    public TodoListAdapter(LinkedList<Todo> dataset, Context context){
        this.inflater = LayoutInflater.from(context);
        this.dataset = dataset;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = inflater.inflate(R.layout.check_list_item, viewGroup, false);
        Holder holder = new Holder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        Todo selected = this.dataset.get(i);
        holder.setDatas(selected);
    }

    @Override
    public int getItemCount() {
        return this.dataset.size();
    }
}
