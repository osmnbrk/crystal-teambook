package com.uludag.berkay.crystalteambook.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uludag.berkay.crystalteambook.Models.Personal_Note;
import com.uludag.berkay.crystalteambook.R;
import com.uludag.berkay.crystalteambook.ReadNoteActivity;

import java.util.LinkedList;

class PersonalNoteHolder extends RecyclerView.ViewHolder{

    TextView title;
    TextView message;
    View item;

    public PersonalNoteHolder(@NonNull View itemView) {
        super(itemView);
        item = itemView;
        title = itemView.findViewById(R.id.personal_note_title);
        message = itemView.findViewById(R.id.personal_note_message_sum);
    }

    public void setDatas(Personal_Note data, Context parent){
        this.title.setText(data.id);
        this.message.setText(data.data);

        itemView.setOnClickListener((View v) -> {
            Intent intent = new Intent(parent, ReadNoteActivity.class);
            intent.putExtra("title", data.id);
            intent.putExtra("data", data.data);
            parent.startActivity(intent);
        });
    }
}


public class PersonalNotesAdapter extends RecyclerView.Adapter<PersonalNoteHolder> {
    LayoutInflater layoutInflater;
    LinkedList<Personal_Note> dataset;
    Context context;

    public PersonalNotesAdapter(LinkedList<Personal_Note> dataset, Context context){
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.dataset = dataset;
    }

    @NonNull
    @Override
    public PersonalNoteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = this.layoutInflater.inflate(R.layout.personalnote_card, viewGroup, false);
        PersonalNoteHolder holder = new PersonalNoteHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonalNoteHolder personalNoteHolder, int i) {
        Personal_Note selected = this.dataset.get(i);
        personalNoteHolder.setDatas(selected, context);
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }
}
