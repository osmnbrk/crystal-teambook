package com.uludag.berkay.crystalteambook;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import java.util.Objects;

public class SplashActivity extends AppCompatActivity {

    private ImageView logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Objects.requireNonNull(getSupportActionBar()).hide();

        logo = this.findViewById(R.id.splash_logo);
        logo.setOnClickListener((View v)->{
            Intent intent = new Intent(this, LoginActivity.class);
            this.startActivity(intent);
        });

    }
}
