package com.uludag.berkay.crystalteambook;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.uludag.berkay.crystalteambook.Adapters.PersonalNotesAdapter;
import com.uludag.berkay.crystalteambook.Models.Personal_Note;

import java.util.LinkedList;

public class PersonelNotes extends Fragment {

    RecyclerView container;

    public PersonelNotes() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_personel_notes, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        container = this.getActivity().findViewById(R.id.personal_note_container);

        LinkedList<Personal_Note> dataset = new LinkedList<>();

        dataset.add(new Personal_Note("Başlık 1", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin lacinia tortor eros, eu consequat nulla pulvinar a. Cras nisi neque, suscipit quis tempus et, finibus at libero. Nullam porttitor porttitor arcu. Donec ac egestas eros. Proin cursus enim neque. Donec nec iaculis lacus. Maecenas at ex vel odio sodales porta. Sed posuere, magna vitae efficitur tristique, ante magna fringilla ex, ut ultricies dolor risus vitae magna. Praesent id ullamcorper tortor, at placerat dui"));
        dataset.add(new Personal_Note("Başlık 2", "Morbi placerat faucibus ante. In risus dui, hendrerit in accumsan id, vulputate eget orci. Nulla tristique faucibus ex id tincidunt. Curabitur facilisis mi quis felis tincidunt, at faucibus quam mollis. Duis non tempor arcu. Praesent eu ornare dolor. Cras tortor enim, bibendum a volutpat id, commodo quis lorem. Praesent bibendum consequat neque, eget congue risus sollicitudin ac. Phasellus odio tortor, maximus ut varius iaculis, vestibulum eu neque."));
        dataset.add(new Personal_Note("Başlık 3", "Donec eu dui vulputate, condimentum nibh at, eleifend sapien. Donec iaculis justo sed congue molestie. Donec lectus odio, tincidunt vitae luctus a, dictum a ipsum. Proin bibendum enim ac diam volutpat, vel volutpat purus rhoncus. Ut orci erat, aliquam eu pretium in, vulputate in sem. Quisque at ante id sapien ultrices dignissim sed convallis quam. Donec tempus semper neque ut dignissim."));
        dataset.add(new Personal_Note("Başlık 4", "Ut non lacinia ligula. Phasellus eget tempus arcu. Morbi non mi et neque condimentum finibus. Fusce commodo est odio. Aenean sodales libero in urna sollicitudin, interdum ultrices mi fringilla. Nunc neque lectus, semper quis nulla ut, pharetra rutrum elit. Fusce bibendum elementum arcu ac accumsan. Curabitur commodo ipsum justo, et scelerisque leo pulvinar at. Curabitur sed scelerisque nibh. Etiam sagittis ante nec porta eleifend. Fusce vitae ultrices ipsum."));
        dataset.add(new Personal_Note("Başlık 5", "Duis et blandit odio, sit amet dictum nulla. Nullam nec nibh dictum, interdum elit nec, imperdiet turpis. Maecenas erat turpis, hendrerit eu ullamcorper eu, auctor id libero. Sed commodo erat nec sapien fermentum, eu porta neque vulputate. Phasellus malesuada lorem commodo magna maximus, at maximus nulla posuere. Nunc neque ante, accumsan vel velit sit amet, scelerisque sollicitudin massa. "));
        dataset.add(new Personal_Note("Başlık 6", "Pellentesque sagittis vel turpis non fringilla. Morbi nec nisl dignissim, bibendum massa quis, ultricies purus. Etiam facilisis porta mauris, quis ornare libero interdum ultricies. Maecenas non vestibulum quam. Aliquam id egestas justo, vitae malesuada magna. Nullam fringilla turpis et dictum condimentum. Proin condimentum malesuada nisi eget interdum. Donec pharetra ullamcorper purus, id gravida felis scelerisque ac."));
        dataset.add(new Personal_Note("Başlık 7", "Sed pulvinar mollis interdum. Sed tristique enim ac ullamcorper vehicula. Phasellus erat sem, luctus nec eleifend vel, convallis in diam. In eu leo ac libero ultrices tincidunt. Pellentesque sagittis vestibulum sapien, at tempus ante ullamcorper fermentum. Nam malesuada aliquet nisl et convallis. Etiam pretium, urna vel blandit aliquet, diam sem porta sem, quis tempus dolor lorem vitae arcu. Etiam eget risus tincidunt, tincidunt risus gravida, porttitor tortor. Fusce efficitur pellentesque augue ut molestie. Donec ut porttitor tortor. "));
        dataset.add(new Personal_Note("Başlık 8", "Proin rutrum vitae leo nec consectetur. Maecenas a ipsum dolor. Morbi at tellus consectetur, tincidunt dui id, pharetra purus. Suspendisse non erat sed arcu feugiat ultricies. Fusce ullamcorper a felis nec vestibulum. Donec mollis, augue ac blandit vulputate, magna libero convallis neque, non lacinia nisl metus quis libero. Vivamus vehicula, magna vel mattis hendrerit, ipsum diam condimentum magna, a finibus ipsum nibh sit amet leo. "));
        dataset.add(new Personal_Note("Başlık 9", "Cras rutrum metus erat, id pulvinar nulla rutrum ut. Nam finibus ante dolor, non luctus tellus suscipit non. Donec in ultricies odio. Ut euismod diam nunc, ut fermentum sapien congue a. Etiam mollis magna quis convallis feugiat. In blandit erat lorem, vel fringilla orci cursus ut. Morbi auctor ultricies dolor a ornare. In sodales quam non elit malesuada suscipit. Morbi pulvinar risus diam, sit amet tristique tortor cursus et. Sed a finibus erat. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer vitae tristique ligula, sit amet pellentesque velit. "));

        PersonalNotesAdapter adapter = new PersonalNotesAdapter(dataset, this.getContext());
        container.setAdapter(adapter);

        GridLayoutManager manager = new GridLayoutManager(this.getContext(), 2);
        container.setLayoutManager(manager);


    }
}
