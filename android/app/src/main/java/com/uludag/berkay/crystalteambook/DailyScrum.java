package com.uludag.berkay.crystalteambook;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.uludag.berkay.crystalteambook.Adapters.TodoListAdapter;
import com.uludag.berkay.crystalteambook.Models.Todo;

import java.util.LinkedList;


public class DailyScrum extends Fragment {

    RecyclerView container;

    public DailyScrum() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.daily_scrums, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        container = this.getActivity().findViewById(R.id.checklist_container);

        LinkedList<Todo> dataset = new LinkedList<>();

        dataset.add(new Todo("Lorem Ipsum donor sit amet", "onProgress"));
        dataset.add(new Todo("Lorem Ipsum donor sit amet", "onProgress"));
        dataset.add(new Todo("Veritabanı Bağlantı", "onProgress"));
        dataset.add(new Todo("Durasyon", "onProgress"));

        TodoListAdapter adapter = new TodoListAdapter(dataset, getContext());
        container.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        container.setLayoutManager(linearLayoutManager);




    }
}
