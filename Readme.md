# Crystal Teambook
** Please read our project document if you are interested **
[Project Document](./media/Crystal_Report.pdf)

 

## What is Crystal Teambook?
First of all, Crystal Teambook is a small homework project for Uludag University Computer Science which designed and coded by: 

- Yağız Türkmen 
- Osman Berk Aşık
- Berkin Erdem
- Berkay Dedeoğlu

 Crystal Teambook is project management and communication system designed for Agile (especially Scrum) teams.

## Features
### Personal Notes
You can store your basic personal notes via this application.

<center>
	<img alt="PersonalNotes" src="./media/personal_notes.png" width=256 />
</center>

Then you need them whenever you want. (Internet connection required)

<center> 
	<img alt="PersonalNotes" src="./media/personal_notes_detail.png" width=256 />
</center>

### Team Board
You can track your team and your project with Crystal Teambook.
 
## Architechure?

Crystal Teambook is actually an Android application which is always connected our web services.

And our **web services** means 

- AWS Api Gateway
- AWS DynamoDb
- AWS Lambdas 

We stored your plans, daily scrum notes and personal notes in _DynamoDb_ and we show whenever you need them. Also we put them via a Restfull Api (Api Gateway) and Lambdas.

Our native Android application coded with pure Java.

If you want more deteails, please read [the document](./media/Crystal_Report.pdf).

